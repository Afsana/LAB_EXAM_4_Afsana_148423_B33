<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Picture Upload Modal</h2>
    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">click for  Modal</button>

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                    <h4 class="modal-title"> Picture Upload </h4>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading"><h2 align="center" >bootstrap</h2></div>
                    <form action="file.php" method="post"  enctype = "multipart/form-data">
                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="usr">Picture Uploading.....</label>
                                        <input type="file" class="form-control-file" id="fileToUpload"  name="fileToUploaded" required>
                                    </div>
                                    <div class="form-group">

                                        <label for="usr">Description of File: </label>
                                        <textarea class="form-control" placeholder="Message" rows="6" name="Description" required></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="usr">Date:</label><br>
                                        <input type="date" placeholder="date" name="date" required>
                                    </div>

                                    <button type="submit" class="btn btn-danger">Send</button>

                                </div>
                            </div>


                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">close</button>

                </div>
            </div>

        </div>
    </div>

</div>

</body>
</html>